<?php
session_start();
if (!isset($_SESSION['user'])) {
    header("location: index.html");
}
if (isset($_POST['sendmsg'])) {
    $con = mysqli_connect("wa.toad.cz", "savvavic", "webove aplikace", "savvavic");
    $msg = htmlspecialchars($_POST['msg']);
    $user = $_SESSION['user'];
    date_default_timezone_set("Europe/Prague");
    $date = date("d.M.Y H:i:s");
    $query2 = "INSERT INTO chat(date,user,msg) VALUES ('$date','$user','$msg')";
    $res=mysqli_query($con,$query2);
    header("location:chat.php");
}
if (isset($_POST['logout'])) {
    unset($_SESSION['user']);
    session_destroy();
    header("location:index.html");
}
else { ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>CHATOPOLIA</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="chatindexstyles.css" type="text/css"/>
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script>
            $(document).ready(function(){
                setInterval(function() {
                    $('.chat').load('chat.php #chat');
                }, 1500);
            });
        </script>
    </head>
    <body>
        <div class="chat">
            <?php
            $con = mysqli_connect("wa.toad.cz", "savvavic", "webove aplikace", "savvavic");
            $user = $_SESSION['user'];
            $query = "SELECT * FROM chat WHERE user != '$user' ORDER BY DATE DESC";
            $res = mysqli_query($con, $query);
            while ($row = mysqli_fetch_array($res)) {
                echo "<h3 id='chat'><span id='date1'>" .$row['date']."</span><br>From: <span id='from'>" . $row['user'] . "</span><br>Message: <br><span id='inc'>".$row['msg']."</span><br></h3>";
            }
            ?>
        </div>
        <div class="chat2">
            <?php
            $con = mysqli_connect("wa.toad.cz", "savvavic", "webove aplikace", "savvavic");
            $user = $_SESSION['user'];
            $query = "SELECT * FROM chat WHERE user = '$user' ORDER BY DATE DESC";
            $res = mysqli_query($con, $query);
            while ($row = mysqli_fetch_array($res)) {
                echo "<h3><span id='date2'>" . $row['date'] . "</span><br>From: <span id='me'>me</span><br>Message: <br><span id='outc'>".$row['msg']."</span><br></h3>";
            }
            ?>
        </div>
        <div class="writesth">
            <form action="chat.php" method="post" name="sendmsg">
                <label for="writesth">Say something: </label>
                <input id="writesth" type="text" name="msg" placeholder="..." autofocus>
                <button type="submit"  name="sendmsg" id="sendmsgbutton">Send</button>
            </form>
        </div>
        <form method="post" name="logout" action="chat.php">
            <button type="submit" name="logout" id="logoutbutton">Sign Out</button>
        </form>
        <form action="profile.php">
            <button type="submit" id="profile">Profile</button>
        </form>
    </body>
</html>
<?php } ?>
